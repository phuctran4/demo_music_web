
$(function () {
    $.get("http://localhost:8080/account/show", function (data, status) {
        console.log(data);
        console.log(status);
        $("tbody").empty();

        let tbody = "";
        for (let i = 0; i < data.length; i++) {
            tbody += "<tr>";
            tbody += "<td>" + data[i].id + "</td>";
            tbody += "<td><button type='button' class='btn btn-link' data-account-id='" + data[i].id + "' onclick='openModalAndFillData(" + data[i].id + ")'>" + data[i].username + "</button></td>";
            tbody += "<td>" + data[i].email + "</td>";
            tbody += "<td>" + data[i].fullName + "</td>";
            tbody += "<td>" + data[i].role + "</td>";
            tbody += "<td><button class='btn btn-outline-light text-dark " + data[i].id + "' data-account-id='" + data[i].id + "' onclick='deleteData(this)' type='button'><i class=\"fa-solid fa-trash fa-fade\" style=\"color: #28144d;\"></i></button></td>";
            tbody += "</tr>";
            tbody += "</tr>";
        }
        $("tbody").append(tbody);
    });
});


// Function to fetch account data and populate the modal
function openModalAndFillData(id) {
    $.get("http://localhost:8080/account/show/" + id, function (data, status) {
        console.log(data);
        console.log(status);
        if (data) {
            // Điền thông tin tài khoản vào các trường input trong modal
            $("#username").val(data.username);
            $("#email").val(data.email);
            $("#password").val(data.password);
            $("#fullName").val(data.fullName);
            $("#role").val(data.role);

            // Show the modal for updating the account
            showModal("update", id, data.username,data.email,data.fullName,data.role);
        } else {
            alert("Account with id not found: " + id);
        }
    });
}

// Function to show the modal and handle the form submission for adding a new account
function addAccount() {
    let username = $("#username").val();
    let email = $("#email").val();
    let password = $("#password").val();
    let fullName = $("#fullName").val();
    let role = $("#role").val();

    let account = {
        username: username,
        email: email,
        password: password,
        fullName: fullName,
        role: role
    };

    $.ajax({
        type: 'POST',
        url: 'http://localhost:8080/account',
        data: JSON.stringify(account),
        contentType: "application/json; charset=utf-8",
        success: function (data, status) {
            console.log("successfully");
            console.log(data);
            console.log(status);
            window.location.href = "/listAccount";
        },
        error: function (data, status) {
            console.log(data);
            console.log(status);
            alert("Please re-enter your account information");
        }
    });
}

// Function to show the modal and handle the form submission for updating an existing account

function updateAccount() {
    let username = $("#username").val();
    let email = $("#email").val();
    let password = $("#password").val();
    let fullName = $("#fullName").val();
    let role = $("#role").val();
    const id = $("#save").data("data-account-id");

    let account = {
        username: username,
        email: email,
        password: password,
        fullName: fullName,
        role: role,
        id: id // Sử dụng trực tiếp biến id
    };

    $.ajax({
        type: 'PUT',
        url: 'http://localhost:8080/account',
        data: JSON.stringify(account),
        contentType: "application/json; charset=utf-8",
        success: function (data, status) {
            console.log("successfully");
            console.log(data);
            console.log(status);
            alert("Update successfully");
            window.location.href = "/listAccount";
        },
        error: function (data, status) {
            console.log(data);
            console.log(status);
            alert("Please enter valid account information");
        }
    });
}

// Event listener for the "Submit" button in the modal
$("#save").click(function () {

    const modalType = $(this).data("modal-type");
    let errors = [];
    const username = $("#username").val();
    const password = $("#password").val();
    const confirmPassword = $('#confirmPassword').val();

    if (!username || username === "") {
        errors.push("emptyUsername");
    }

    // case create check password null, blank;
    if (modalType === 'add') {
        if (!password || password === "") {
            errors.push("emptyPassword");
        }
        if (!confirmPassword || confirmPassword === "" || confirmPassword !== password) {
            errors.push("confirmPasswordNotCorrect");
        }
    }

    // case edit
    if (modalType === 'update') {
        if (password !== confirmPassword || (password === "" && confirmPassword !== "") || (password !== "" && confirmPassword === "")) {
            errors.push("confirmPasswordNotCorrect");
        }
    }
    if (errors.length > 0) {
        let errorMessage = "Please correct the following errors:\n";
        for (let i = 0; i < errors.length; i++) {
            switch (errors[i]) {
                case "emptyUsername":
                    errorMessage += "- Username cannot be empty\n";
                    break;
                case "emptyPassword":
                    errorMessage += "- Password cannot be empty\n";
                    break;
                case "confirmPasswordNotCorrect":
                    errorMessage += "- Passwords do not match\n";
                    break;
                // Add more error cases if needed
            }
        }
        alert(errorMessage);
        return; // Stop the submission if there are errors
    }else {
        if (modalType === "add") {
            addAccount();
        } else if (modalType === "update") {
            updateAccount();
        }
    }
});

// Function to show the modal and set the appropriate modal type
function showModal(modalType,id, username,email,fullName) {
    $("#username").val(username);
    $("#email").val(email);
    $("#fullName").val(fullName);
    $("#save").data("modal-type", modalType);
    $("#save").data("data-account-id", id);
    $("#myModal").modal("show");
}

function deleteData(obj) {
    console.log(obj);
    console.log(obj.className);

    // Show the custom modal for confirmation
    $('#deleteConfirmationModal').modal('show');

    // Attach event listener to the "Yes" button
    $('#deleteConfirmBtn').one('click', function () {
        // Close the confirmation modal
        $('#deleteConfirmationModal').modal('hide');

        const accountId = $(obj).data("account-id");
        // Perform the delete operation
        $.ajax({
            type: 'DELETE',
            url: 'http://localhost:8080/account/delete/' + accountId,
            contentType: "application/json; charset=utf-8",
            success: function (data, status) {
                console.log("delete successfully");

                // Reload the account data after successful deletion
                $.get("http://localhost:8080/account/show", function (data, status) {
                    console.log(data);
                    console.log(status);
                    $("tbody").empty();

                    let tbody = "";
                    for (let i = 0; i < data.length; i++) {
                        tbody += "<tr>";
                        tbody += "<td>" + data[i].id + "</td>";
                        tbody += "<td><button type='button' class='btn btn-link' data-account-id='" + data[i].id + "' onclick='openModalAndFillData(" + data[i].id + ")'>" + data[i].username + "</button></td>";
                        tbody += "<td>" + data[i].email + "</td>";
                        tbody += "<td>" + data[i].fullName + "</td>";
                        tbody += "<td>" + data[i].role + "</td>";
                        tbody += "<td><button class='btn btn-outline-light text-dark " + data[i].id + "' data-account-id='" + data[i].id + "' onclick='deleteData(this)' type='button'><i class=\"fa-solid fa-trash fa-fade\" style=\"color: #28144d;\"></i></button></td>";
                        tbody += "</tr>";
                        tbody += "</tr>";
                    }
                    $("tbody").append(tbody);
                });
            },
            error: function (data, status) {
                console.log(data);
                console.log(status);
            }
        });
    });
}

$(function () {
    $("#save").click(function () {
        let username = $("#username").val();
        let email = $("#email").val();
        let password = $("#password").val();
        let fullName = $("#fullName").val();
        let role = "USER";

        let account = {
            username: username,
            email: email,
            password: password,
            fullName: fullName,
            role: role
        };

        let errors = [];

        const confirmPassword = $('#confirmPassword').val();

        if (!username || username === "") {
            errors.push("emptyUsername");
        }

        if (!password || password === "") {
            errors.push("emptyPassword");
        }

        if (!confirmPassword || confirmPassword === "" || confirmPassword !== password) {
            errors.push("confirmPasswordNotCorrect");
        }

        if (errors.length > 0) {
            let errorMessage = "Please correct the following errors:\n";
            for (let i = 0; i < errors.length; i++) {
                switch (errors[i]) {
                    case "emptyUsername":
                        errorMessage += "- Username cannot be empty\n";
                        break;
                    case "emptyPassword":
                        errorMessage += "- Password cannot be empty\n";
                        break;
                    case "confirmPasswordNotCorrect":
                        errorMessage += "- Passwords do not match\n";
                        break;
                    // Add more error cases if needed
                }
            }
            alert(errorMessage);
            return;
        }

        $.ajax({
            type: 'POST',
            url: 'http://localhost:8080/registration',
            data: JSON.stringify(account),
            contentType: "application/json; charset=utf-8",
            success: function (data, status) {
                console.log("successfully");
                console.log(data);
                console.log(status);

                alert("create account successfully, go to sign in please!!");
                window.location.href = "/login";
            },
            error: function (data, status) {
                console.log(data);
                console.log(status);
                alert("Please re-enter your account information");
            }
        });
    });
});

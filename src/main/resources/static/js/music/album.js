$(document).ready(function () {
    // Biến toàn cục
    let currentCategoryId = 1;
    let currentSongId = 0;
    let currentSongIndex = -1;
    let isRandom = false;
    let isRepeating = false;
    let data = []; // Khởi tạo data như một mảng rỗng

    // Function để cập nhật currentSongIndex dựa trên danh sách bài hát hiện tại cho currentCategoryId
    function updateCurrentSongIndex() {
        for (let i = 0; i < data.length; i++) {
            if (data[i].id === currentSongId && data[i].categoryId === currentCategoryId) {
                currentSongIndex = i;
                break;
            }
        }
    }

// Function để chuyển đổi categoryId và cập nhật lại currentSongIndex
    function switchCategory(newCategoryId) {
        currentCategoryId = newCategoryId;
        // Cập nhật lại currentSongIndex dựa trên danh sách bài hát cho newCategoryId
        for (let i = 0; i < data.length; i++) {
            if (data[i].id === currentSongId && data[i].categoryId === currentCategoryId) {
                currentSongIndex = i;
                break;
            }
        }
    }


    // Function để phát bài hát tiếp theo
    function playNextSong() {
        let nextSongIndex = (currentSongIndex + 1) % data.length;
        while (data[nextSongIndex].categoryId !== currentCategoryId) {
            nextSongIndex = (nextSongIndex + 1) % data.length;
        }
        currentSongIndex = nextSongIndex;
        const nextSongId = data[currentSongIndex].id;
        openModalAndFillData(nextSongId);
    }

    // Function để phát bài hát trước đó
    function playPreviousSong() {
        let prevSongIndex = (currentSongIndex - 1 + data.length) % data.length;
        while (data[prevSongIndex].categoryId !== currentCategoryId) {
            prevSongIndex = (prevSongIndex - 1 + data.length) % data.length;
        }
        currentSongIndex = prevSongIndex;
        const prevSongId = data[currentSongIndex].id;
        openModalAndFillData(prevSongId);
    }

    // Function để phát lại bài hát hiện tại
    function repeatCurrentSong() {
        const audioElement = document.getElementById("audio");
        audioElement.currentTime = 0; // Quay lại thời gian bắt đầu của bài hát
        audioElement.play();
    }

    // Function để phát bài hát ngẫu nhiên
    function playRandomSong() {
        const randomIndex = Math.floor(Math.random() * data.length);
        const randomSongId = data[randomIndex].id;
        openModalAndFillData(randomSongId);
    }

    // Function để cập nhật thanh tiến độ bài hát
    function updateProgressBar() {
        const audioElement = document.getElementById("audio");
        const progressBar = document.getElementById("progress");

        audioElement.addEventListener("timeupdate", function () {
            const currentTime = audioElement.currentTime;
            const duration = audioElement.duration;

            if (isFinite(currentTime) && isFinite(duration)) {
                const progressPercentage = (currentTime / duration) * 100;
                progressBar.value = progressPercentage;
            }
        });

        progressBar.addEventListener("input", function () {
            const progressPercentage = progressBar.value;
            const duration = audioElement.duration;

            if (isFinite(duration)) {
                const newTime = (progressPercentage / 100) * duration;
                audioElement.currentTime = newTime;
            }
        });
    }

    // Sự kiện click nút "Next"
    $(".btn-next").click(function () {
        if (isRandom) {
            playRandomSong();
        } else {
            playNextSong();
        }
        updateCurrentSongIndex();
    });

    // Sự kiện click nút "Previous"
    $(".btn-prev").click(function () {
        if (isRandom) {
            playRandomSong();
        } else {
            playPreviousSong();
        }
        updateCurrentSongIndex();
    });

    // Sự kiện click nút "Repeat"
    $(".btn-repeat").click(function () {
        isRepeating = !isRepeating;

        const repeatButton = $(this);
        const repeatIcon = repeatButton.find("i");

        if (isRepeating) {
            repeatButton.addClass("active");
        } else {
            repeatButton.removeClass("active");
        }
    });

    // Sự kiện 'ended' trên phần tử <audio>
    const audioElement = document.getElementById("audio");
    audioElement.addEventListener("ended", function () {
        if (isRandom) {
            playRandomSong();
        } else if (isRepeating) {
            repeatCurrentSong();
        } else {
            playNextSong();
        }
    });

    // Sự kiện click nút "Random"
    $(".btn-random").click(function () {
        isRandom = !isRandom;
        const randomButton = $(this);
        const randomIcon = randomButton.find("i");

        if (isRandom) {
            randomButton.addClass("active");
        } else {
            randomButton.removeClass("active");
        }
    });

    $("#fetchDataButton1").click(function () {
        switchCategory(1);
        $.get("http://localhost:8080/music/album/1", function (responseData) {
            data = responseData; // Gán dữ liệu trả về từ server cho biến data
            populateTable(data, $("#musicTableBody1"));
        });
    });

    $("#fetchDataButton2").click(function () {
        switchCategory(2);
        $.get("http://localhost:8080/music/album/2", function (responseData) {
            data = responseData;
            populateTable(data, $("#musicTableBody2"));
        });
    });

    $("#fetchDataButton3").click(function () {
        switchCategory(3);
        $.get("http://localhost:8080/music/album/3", function (responseData) {
            data = responseData;
            populateTable(data, $("#musicTableBody3"));
        });
    });

    // Hàm để lấy dữ liệu âm nhạc và cập nhật giao diện
    function populateTable(data, tbody) {
        tbody.empty(); // Xóa dữ liệu cũ

        for (let i = 0; i < data.length; i++) {
            if (data[i].categoryId === currentCategoryId) {
                const newRow = "<tr>" +
                    "<td><button type='button' class='btn btn-link' data-account-id='" + data[i].id + "' onclick='openModalAndFillData(" + data[i].id + ")'>" + data[i].musicName + "</button></td>" +
                    "<td>" + data[i].artist + "</td>" +
                    "<td>" + data[i].categoryName + "</td>" +
                    "</tr>";

                tbody.append(newRow);
            }
        }
    }
    updateProgressBar();
});
//mở modal
function openModalAndFillData(id) {
    $.get("http://localhost:8080/music/show/" + id, function (data, status) {
        console.log(data);
        console.log(status);
        if (data) {
            // Update the audio source and play the audio
            if (data.audioFilePath) {
                const cdThumbElement = document.querySelector(".cd-thumb");
                const playPauseButton = $("#playPauseButton");
                const playIcon = playPauseButton.find(".fa-play");
                const pauseIcon = playPauseButton.find(".fa-pause");

                cdThumbElement.dataset.imgSrc = data.imgFilePath;
                cdThumbElement.style.backgroundImage = `url(${data.imgFilePath})`;

                const audioElement = document.getElementById("audio");
                audioElement.src = data.audioFilePath;
                audioElement.play();
                playIcon.hide();
                pauseIcon.show();
                cdThumbElement.classList.add("cd-spin");

                // Cập nhật đường dẫn ảnh từ dữ liệu vào thuộc tính data-img-path của nút Play/Pause
                playPauseButton.attr("data-img-path", data.imgFilePath);
            }

            // Fill other modal fields
            $("#musicName").val(data.musicName);
            $("#artist").val(data.artist);
            $("#categoryName").val(data.categoryName);
            $("#musicTitle").text(data.musicName);
            // Show the modal for updating the account
            $("#myModal").modal("show");
        } else {
            alert("Music with ID not found: " + id);
        }
    });
}

//pause and play nhạc
$("#playPauseButton").click(function() {
    const cdThumbElement = document.querySelector(".cd-thumb");
    const audioElement = document.getElementById("audio");
    const playPauseButton = $(this);
    const playIcon = playPauseButton.find(".fa-play");
    const pauseIcon = playPauseButton.find(".fa-pause");
    const imgFilePath = playPauseButton.attr("data-img-path");



    if (audioElement.paused) {
        audioElement.play();
        playIcon.hide();
        pauseIcon.show();
        cdThumbElement.classList.add("cd-spin");

        // Cập nhật background-image của CD từ imgFilePath
        cdThumbElement.style.backgroundImage = `url(${imgFilePath})`;
    } else {
        audioElement.pause();
        playIcon.show();
        pauseIcon.hide();
        cdThumbElement.classList.remove("cd-spin");

    }
});
//điều chỉnh tiến độ bài hát và tua nhạc
const audioElement = document.getElementById("audio");
const progressBar = document.getElementById("progress");

audioElement.addEventListener("timeupdate", function () {
    const currentTime = audioElement.currentTime;
    const duration = audioElement.duration;

    if (isFinite(currentTime) && isFinite(duration)) {
        const progressPercentage = (currentTime / duration) * 100;
        progressBar.value = progressPercentage;
    }
});

progressBar.addEventListener("input", function () {
    const progressPercentage = progressBar.value;
    const duration = audioElement.duration;

    if (isFinite(duration)) {
        const newTime = (progressPercentage / 100) * duration;
        audioElement.currentTime = newTime;
    }
});
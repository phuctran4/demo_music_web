$(document).ready(function () {
    // Initialization code when the document is ready

    // Define and initialize variables
    let isRandom = false;
    let isRepeating = false;
    let data=[];
    $.get("http://localhost:8080/music/show", function (responseData, status) {
        console.log(responseData);
        console.log(status);
        $("tbody").empty();
        data=responseData
        let tbody = "";
        for (let i = 0; i < data.length; i++) {
            tbody += "<tr>";
            tbody += "<td><button type='button' class='btn btn-link' data-account-id='" + data[i].id + "' onclick='openModalAndFillData(" + data[i].id + ")'>" + data[i].musicName + "</button></td>";
            tbody += "<td>" + data[i].artist + "</td>";
            tbody += "<td>" + data[i].categoryName + "</td>";
            tbody += "<td>";
            tbody += '<div class="dropdown">';
            tbody += '<button type="button" class="btn btn-outline-light text-dark" data-bs-toggle="dropdown">';
            tbody += '<i class="fa-solid fa-ellipsis-vertical"></i>';
            tbody += '</button>';
            tbody += '<ul class="dropdown-menu">';
            tbody += '<li><a class="dropdown-item" href="#">Link 1</a></li>';
            tbody += '<li><a class="dropdown-item" href="#">Link 2</a></li>';
            tbody += '<li><a class="dropdown-item" href="#">Link 3</a></li>';
            tbody += '</ul>';
            tbody += '</div>';
            tbody += "</td>";
            tbody += "</tr>";

        }
        $("tbody").append(tbody);
    });

    let currentSongIndex=-1;

    //next và prev bài hát
    function playNextSong() {
        currentSongIndex = (currentSongIndex + 1 ) % data.length;
        const nextSongId = data[currentSongIndex].id;
        openModalAndFillData(nextSongId);
    }

    function playPreviousSong() {
        currentSongIndex = (currentSongIndex - 1 + data.length) % data.length;
        const prevSongId = data[currentSongIndex].id;
        openModalAndFillData(prevSongId);
    }

    // Gán sự kiện cho nút "Next" và "Previous".
    $(".btn-next").click(function () {
        if (isRandom) {
            playRandomSong();
        } else {
            playNextSong();
        }
    });

    $(".btn-prev").click(function () {
        if (isRandom) {
            playRandomSong();
        } else {
            playPreviousSong();
        }
    });
    // Function to repeat the current song
    function repeatCurrentSong() {
        const audioElement = document.getElementById("audio");
        audioElement.currentTime = 0; // Restart the current song
        audioElement.play();
    }

    // Event handler for the "Repeat" button click
    $(".btn-repeat").click(function () {
        isRepeating = !isRepeating;
        const repeatButton = $(this);
        const repeatIcon = repeatButton.find("i");

        if (isRepeating) {
            repeatButton.addClass("active");
        } else {
            repeatButton.removeClass("active");
        }
    });

    // Event handler for the "ended" event on the audio element
    const audioElement = document.getElementById("audio");
    audioElement.addEventListener("ended", function () {
        if (isRandom) {
            playRandomSong();
        } else if (isRepeating) {
            repeatCurrentSong();
        } else {
            playNextSong();
        }
    });

    // Function to play a random song
    function playRandomSong() {
        const randomIndex = Math.floor(Math.random() * data.length);
        const randomSongId = data[randomIndex].id;
        openModalAndFillData(randomSongId);
    }

    // Event handler for the "Random" button click
    $(".btn-random").click(function () {
        isRandom = !isRandom;
        const randomButton = $(this);
        const randomIcon = randomButton.find("i");

        if (isRandom) {
            randomButton.addClass("active");
        } else {
            randomButton.removeClass("active");
        }
    });
});

function openModalAndFillData(id) {
    $.get("http://localhost:8080/music/show/" + id, function (data, status) {
        console.log(data);
        if (data) {
            // Update the audio source and play the audio
            if (data.audioFilePath) {
                const cdThumbElement = document.querySelector(".cd-thumb");
                const playPauseButton = $("#playPauseButton");
                const playIcon = playPauseButton.find(".fa-play");
                const pauseIcon = playPauseButton.find(".fa-pause");

                // Update CD thumb image
                cdThumbElement.dataset.imgSrc = data.imgFilePath;
                cdThumbElement.style.backgroundImage = `url(${data.imgFilePath})`;

                // Update audio source
                audioElement.src = data.audioFilePath;
                audioElement.play();
                playIcon.hide();
                pauseIcon.show();
                cdThumbElement.classList.add("cd-spin");

                // Update data-img-path attribute for Play/Pause button
                playPauseButton.attr("data-img-path", data.imgFilePath);
            }

            // Fill other modal fields
            $("#musicName").val(data.musicName);
            $("#artist").val(data.artist);
            $("#categoryName").val(data.categoryName);
            $("#musicTitle").text(data.musicName);

            // Show the modal for updating the account
            $("#myModal").modal("show");
        } else {
            alert("Music with ID not found: " + id);
        }
    });
}

// Event handler for the Play/Pause button click
$("#playPauseButton").click(function () {
    const cdThumbElement = document.querySelector(".cd-thumb");
    const playPauseButton = $(this);
    const playIcon = playPauseButton.find(".fa-play");
    const pauseIcon = playPauseButton.find(".fa-pause");
    const imgFilePath = playPauseButton.attr("data-img-path");

    if (audioElement.paused) {
        // If audio is paused, play it
        audioElement.play();
        playIcon.hide();
        pauseIcon.show();
        cdThumbElement.classList.add("cd-spin");
        cdThumbElement.style.backgroundImage = `url(${imgFilePath})`;
    } else {
        // If audio is playing, pause it
        audioElement.pause();
        playIcon.show();
        pauseIcon.hide();
        cdThumbElement.classList.remove("cd-spin");
    }
});

// Event listener for updating the progress bar as the song plays
const audioElement = document.getElementById("audio");
audioElement.addEventListener("timeupdate", function () {
    const currentTime = audioElement.currentTime;
    const duration = audioElement.duration;

    if (isFinite(currentTime) && isFinite(duration)) {
        const progressPercentage = (currentTime / duration) * 100;
        progressBar.value = progressPercentage;
    }
});

// Event listener for user input on the progress bar
const progressBar = document.getElementById("progress");
progressBar.addEventListener("input", function () {
    const progressPercentage = progressBar.value;
    const duration = audioElement.duration;

    if (isFinite(duration)) {
        const newTime = (progressPercentage / 100) * duration;
        audioElement.currentTime = newTime;
    }
});
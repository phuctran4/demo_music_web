$(function () {
    $.get("http://localhost:8080/music/show", function (data, status) {
        console.log(data);
        console.log(status);
        displayMusicData(data);
    });

function displayMusicData(data) {
        $("tbody").empty();

        let tbody = "";
        for (let i = 0; i < data.length; i++) {
            tbody += "<tr>";
            tbody += "<td>" + data[i].id + "</td>";
            tbody += "<td>" + data[i].musicName + "</td>";
            tbody += "<td>" + data[i].artist + "</td>";
            tbody += "<td>" + data[i].categoryName + "</td>";
            tbody += "<td>" + data[i].audioFilePath + "</td>";
            tbody += "<td>" + data[i].imgFilePath + "</td>";
            tbody += "</tr>";
        }
        $("tbody").append(tbody);
    }
});

$(function () {
    $("#searchButton").click(function () {
        const musicName = $("#musicNameInput").val();
        searchMusicByName(musicName);
    });
    function searchMusicByName(name) {
        $.get("http://localhost:8080/music/search", { name: name })
            .done(function (data) {
                console.log(data);
                musicData(data);
            })
            .fail(function (error) {
                console.error("Error:", error);
                handleSearchError();
            });

    }

    function handleSearchError() {
        console.log("Search error occurred.");
        alert("Not find this song ");
    }
    function musicData(data) {
        $("tbody").empty();

        let tbody = "";
            tbody += "<tr>";
            tbody += "<td>" + data.id + "</td>";
            tbody += "<td>" + data.musicName + "</td>";
            tbody += "<td>" + data.artist + "</td>";
            tbody += "<td>" + data.categoryName + "</td>";
            tbody += "<td>" + data.audioFilePath + "</td>";
            tbody += "<td>" + data.imgFilePath + "</td>";
            tbody += "</tr>";

        $("tbody").append(tbody);
    }

});

function handleFileUpload(event) {
    event.preventDefault(); // Prevent default form submission

    const formData = new FormData(event.target); // Get form data

    // AJAX POST request for file upload
    $.ajax({
        type: 'POST',
        url: '/upload',
        data: formData,
        contentType: false, // Let jQuery handle content type
        processData: false, // Don't process data, let jQuery handle it
        success: function (response) {
            console.log('File uploaded successfully:', response);

            // Store the uploaded file name in the global variable
            uploadedFileName = response;

            // Update the audioFilePath field with the uploaded file's name
            $('#audioFilePath').val(uploadedFileName);
        },
        error: function (error) {
            console.error('Error uploading file:', error);
            // Handle error
        }
    });
}


$(function () {
    $("#save").click(function () {
        let id = 0;
        let musicName = $("#musicName").val();
        let categoryId = $("#category").val();
        let artist = $("#artist").val();
        // Use the uploadedFileName variable here
        let audioFilePath = uploadedFileName;
        let imgFilePath = $("#imgFilePath").val();

        let music = {
            id: id,
            musicName: musicName,
            categoryId: categoryId,
            artist: artist,
            imgFilePath: imgFilePath,
            audioFilePath: "/songs/" + audioFilePath
        };

        $.ajax({
            type: 'POST',
            url: 'http://localhost:8080/music',
            data: JSON.stringify(music),
            contentType: "application/json; charset=utf-8",
            success: function (data, status) {
                console.log("successfully");
                console.log(data);
                console.log(status);
                window.location.href = "/edit-music"
            },
            error: function (data, status) {
                console.log(data);
                console.log(status);
                alert("Please re-enter song information")
            }
        });
    });
    $(function () {


        $("#update").click(function () {

            let id = $("#id").val();
            let updateMusicName = $("#updateMusicName").val();
            let updateArtist = $("#updateArtist").val();
            let updateAudioFilePath = uploadedFileName;
            let UpdateImgFilePath = $("#UpdateImgFilePath").val();
            let UpdateCategory = $("#UpdateCategory").val();

            let music={
                id:id,
                musicName:updateMusicName,
                categoryId:UpdateCategory,
                artist:updateArtist,
                imgFilePath:UpdateImgFilePath,
                audioFilePath:"/songs/"+updateAudioFilePath
            };

            $.ajax({
                type: 'PUT',
                url: 'http://localhost:8080/music',
                data: JSON.stringify(music),
                contentType: "application/json; charset=utf-8",
                success: function (data, status) {
                    console.log("successfully");
                    console.log(data);
                    console.log(status);
                    alert("Update successfully");
                    window.location.href="/edit-music";
                },
                error: function (data, status) {
                    console.log(data);
                    console.log(status);
                    alert("Please enter valid song information");
                }
            });
        });
    });

});


    $("#delete").click(function (){
    let deleteById = $("#deleteById").val();

    $.ajax({
        type: 'DELETE',
        url: 'http://localhost:8080/music/' + deleteById,
        data: {},
        contentType: "application/json; charset=utf-8",
        success: function (data, status) {
            console.log("delete successfully");
            console.log(data);
            console.log(status);

            $(function () {
                $.get("http://localhost:8080/music/show", function (data, status) {
                    console.log(data);
                    console.log(status);
                    displayMusicData(data);
                });

                function displayMusicData(data) {
                    $("tbody").empty();

                    let tbody = "";
                    for (let i = 0; i < data.length; i++) {
                        tbody += "<tr>";
                        tbody += "<td>" + data[i].id + "</td>";
                        tbody += "<td>" + data[i].musicName + "</td>";
                        tbody += "<td>" + data[i].artist + "</td>";
                        tbody += "<td>" + data[i].categoryName + "</td>";
                        tbody += "<td>" + data[i].audioFilePath + "</td>";
                        tbody += "<td>" + data[i].imgFilePath + "</td>";
                        tbody += "</tr>";
                    }
                    $("tbody").append(tbody);
                }
            });
        },
        error: function (data, status) {
            console.log(data);
            console.log(status);
        }
    });
        alert("delete fail ",deleteById)
    });

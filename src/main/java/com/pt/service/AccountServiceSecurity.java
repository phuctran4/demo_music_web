package com.pt.service;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface AccountServiceSecurity extends UserDetailsService {
}

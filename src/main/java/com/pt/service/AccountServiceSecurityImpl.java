package com.pt.service;

import com.pt.entity.Account;
import com.pt.repository.AccountRepo;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Optional;

@Service
public class AccountServiceSecurityImpl implements AccountServiceSecurity{

    private final AccountRepo accountRepo;

    public AccountServiceSecurityImpl(AccountRepo accountRepo) {
        this.accountRepo = accountRepo;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Account> opAccount= accountRepo.findByUsername(username);
        if (opAccount.isEmpty()){
            new UsernameNotFoundException("not found Account with username"+ username);
        }

        return new AccountDetailsImpl(opAccount);
    }
}

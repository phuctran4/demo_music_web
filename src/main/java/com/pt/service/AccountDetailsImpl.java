package com.pt.service;

import com.pt.entity.Account;
import com.pt.entity.Role;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Data
public class AccountDetailsImpl implements UserDetails {

    private String username;


    private String password;

    private String email;

    private Role role;


    public AccountDetailsImpl(Optional<Account> account) {

        this.username=account.get().getUsername();

        this.password=account.get().getPassword();
        this.email=account.get().getEmail();
        this.role=account.get().getRole();

    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(role.name()));
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}

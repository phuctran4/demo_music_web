package com.pt.cofigSecurity;

import com.pt.service.AccountServiceSecurity;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RequestFilter extends OncePerRequestFilter {

    private final AccountServiceSecurity accountServiceSecurity;

    public RequestFilter(AccountServiceSecurity accountServiceSecurity) {
        this.accountServiceSecurity = accountServiceSecurity;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        String token=request.getHeader("Authorization");
        if (token==null){

        }
        if (token != null && token.contains("Bearer ")){
            token=token.replace("Bearer ","");
            try {


                Jws<Claims> claimsJws = Jwts.parser().setSigningKey("railway12SecretKey").parseClaimsJws(token);
                String username = claimsJws.getBody().getSubject();
                UserDetails userDetails = accountServiceSecurity.loadUserByUsername(username);
                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities());
                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            }
            catch (ExpiredJwtException e){
                System.out.println(e.getMessage());
            }
        }

        filterChain.doFilter(request,response);

    }
}

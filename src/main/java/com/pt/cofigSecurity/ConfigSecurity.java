package com.pt.cofigSecurity;

import com.pt.service.AccountServiceSecurity;
import lombok.RequiredArgsConstructor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true, jsr250Enabled = true)
public class ConfigSecurity extends WebSecurityConfigurerAdapter {


    private final AccountServiceSecurity accountServiceSecurity;

    public ConfigSecurity(AccountServiceSecurity accountServiceSecurity) {
        this.accountServiceSecurity = accountServiceSecurity;
    }

    @Bean
    public RequestFilter requestFilter(){
        return new RequestFilter(accountServiceSecurity);
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(accountServiceSecurity).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers("/login", "/static/**", "/css/**", "/images/**", "/fonts/**", "/js/**","/songs/**"
                        , "/vendors/**","/registration/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin().loginPage("/login").loginProcessingUrl("/loginProc")
                .defaultSuccessUrl("/").permitAll()
                .failureUrl("/login?success=fail")
                .and()
                .csrf().disable()
                .logout()
                .logoutUrl("/Logout").
                logoutSuccessUrl("/index").permitAll().invalidateHttpSession(true).deleteCookies("JSESSIONID");
                http.addFilterBefore(requestFilter(), UsernamePasswordAuthenticationFilter.class);



        http.cors();
    }
}

package com.pt.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Table
@Entity
public class Music {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String musicName;

    private String artist;

    @NotNull
    private String audioFilePath;


    private String imgFilePath;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

}

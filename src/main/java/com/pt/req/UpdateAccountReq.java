package com.pt.req;


import lombok.Data;

@Data
public class UpdateAccountReq {

    private Long id;

    private String username;

    private String email;

    private String password;

    private String fullName;

    private String role;
}

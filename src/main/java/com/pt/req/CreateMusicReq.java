package com.pt.req;

import com.pt.entity.Category;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CreateMusicReq {

    private Long id;
    @NotNull
    private String musicName;

    private Long categoryId;

    private String artist;

    private String imgFilePath;

    @NotNull
    private String audioFilePath;
}

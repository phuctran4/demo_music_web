package com.pt.req;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UpdateCategoryReq {
    private Long id;

    @NotNull
    private String categoryName;
}

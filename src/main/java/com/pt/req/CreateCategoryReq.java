package com.pt.req;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CreateCategoryReq {
    @NotNull
    private String categoryName;
}

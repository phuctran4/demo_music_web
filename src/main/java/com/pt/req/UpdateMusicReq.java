package com.pt.req;

import com.pt.entity.Category;
import lombok.Data;


import javax.validation.constraints.NotNull;

@Data
public class UpdateMusicReq {


    private Long id;

    @NotNull
    private String musicName;

    private String artist;

    @NotNull
    private String audioFilePath;

    private String imgFilePath;


    private Long categoryId;
}

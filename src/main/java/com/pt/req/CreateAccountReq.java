package com.pt.req;


import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CreateAccountReq {

    private String username;

    private String email;

    private String password;

    private String fullName;

    private String role;
}

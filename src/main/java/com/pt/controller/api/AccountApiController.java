package com.pt.controller.api;

import DTO.AccountDTO;
import com.pt.entity.Account;
import com.pt.entity.Role;
import com.pt.repository.AccountRepo;
import com.pt.req.CreateAccountReq;
import com.pt.req.UpdateAccountReq;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.Optional;

@RequestMapping("/account")
@RestController
public class AccountApiController {

    private final BCryptPasswordEncoder passwordEncoder;

    private final AccountRepo accountRepo;

    private final ModelMapper modelMapper;

    public AccountApiController(AccountRepo accountRepo , ModelMapper modelMapper, BCryptPasswordEncoder passwordEncoder) {
        this.accountRepo = accountRepo;
        this.modelMapper = modelMapper;
        this.passwordEncoder = passwordEncoder;
    }


    @GetMapping("/show")
    public ResponseEntity<?> getAllAccount(){

        List<Account> accountList=accountRepo.findAll();
        List<AccountDTO> accountDTOList=modelMapper.map(accountList,new TypeToken<List<AccountDTO>>(){}.getType());

        return ResponseEntity.ok(accountDTOList);

    }

    @GetMapping("/show/{id}")
    public ResponseEntity<?> getAccountById(@PathVariable Long id) {
        Optional<Account> accountOptional = accountRepo.findById(id);
        if (accountOptional.isPresent()) {
            Account account = accountOptional.get();
            AccountDTO accountDTO = modelMapper.map(account, AccountDTO.class);
            return ResponseEntity.ok(accountDTO);
        }
        return ResponseEntity.badRequest().body("Not found account by id: " + id);
    }


    @PostMapping
    public ResponseEntity<?> save(@RequestBody CreateAccountReq accountReq){
        Optional<Account> optionalAccount=accountRepo.findByUsername(accountReq.getUsername());
        if (optionalAccount.isPresent()){

            return new ResponseEntity<>("account already exists",HttpStatus.BAD_REQUEST);
        }

        accountReq.setPassword(passwordEncoder.encode(accountReq.getPassword()));
        Account account=modelMapper.map(accountReq,Account.class);
        accountRepo.save(account);
        return new ResponseEntity<>("add new successfully",HttpStatus.OK);

    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody UpdateAccountReq updateAccountReq){

        Optional<Account> opAccount=accountRepo.findById(updateAccountReq.getId());

        if (opAccount.isEmpty()){
            return new ResponseEntity<>("Account does not exist!!! not updated: " + updateAccountReq.getId(), HttpStatus.BAD_REQUEST);
        }

        if (updateAccountReq.getPassword()==null || updateAccountReq.getPassword()==""){
            return new ResponseEntity<>("password does not null", HttpStatus.BAD_REQUEST);
        }
        updateAccountReq.setPassword(passwordEncoder.encode(updateAccountReq.getPassword()));
        Account account=modelMapper.map(updateAccountReq,Account.class);
        account.setId(updateAccountReq.getId());
        account.setRole(Role.valueOf(updateAccountReq.getRole()));
        accountRepo.save(account);
        return new ResponseEntity<>("update successfully with id: "+updateAccountReq.getId(), HttpStatus.OK);


    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id){

        Optional<Account> opAccount=accountRepo.findById(id);
        if (opAccount.isPresent()){
            accountRepo.deleteById(id);
            return new ResponseEntity<>("delete successfully : "+ id,HttpStatus.OK);
        }
        return new ResponseEntity<>(" not found id in table",HttpStatus.BAD_REQUEST);
    }
}

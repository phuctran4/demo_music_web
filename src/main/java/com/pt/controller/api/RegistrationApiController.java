package com.pt.controller.api;

import com.pt.entity.Account;
import com.pt.entity.Role;
import com.pt.repository.AccountRepo;
import com.pt.req.CreateAccountReq;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@CrossOrigin(value = "*")
@RestController
public class RegistrationApiController {

    private final BCryptPasswordEncoder passwordEncoder;

    private final AccountRepo accountRepo;

    private final ModelMapper modelMapper;

    public RegistrationApiController(AccountRepo accountRepo, ModelMapper modelMapper,BCryptPasswordEncoder passwordEncoder) {
        this.accountRepo = accountRepo;
        this.modelMapper = modelMapper;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping("/registration")
    public ResponseEntity<?> registration(@RequestBody CreateAccountReq accountReq){

        Optional<Account> opAccount=accountRepo.findByUsername(accountReq.getUsername());
        if (opAccount.isPresent()){

            return ResponseEntity.badRequest().body("account already exist");

        }
        if (accountReq.getPassword()==null){
            return ResponseEntity.badRequest().body("password is null");
        }
        Account account=modelMapper.map(accountReq,Account.class);

        account.setPassword(passwordEncoder.encode(accountReq.getPassword()));

        accountRepo.save(account);



        return ResponseEntity.ok("Sign Up Success");

    }
}

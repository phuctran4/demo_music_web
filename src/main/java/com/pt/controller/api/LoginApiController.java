package com.pt.controller.api;

import com.pt.req.LoginReq;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class LoginApiController {
    private final AuthenticationManager authenticationManager;

    public LoginApiController(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @PostMapping("/login")
    public String login(@RequestBody LoginReq loginReq){

        Authentication authentication=authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginReq.getUsername(),loginReq.getPassword()));

        String jwt= Jwts.builder()
                .setSubject(loginReq.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(new Date((   new Date()).getTime()+1*45*1000))
                .signWith(SignatureAlgorithm.HS512,"railway12SecretKey")
                .compact();


        return jwt;

    }
}


package com.pt.controller.api;

import DTO.AccountDTO;
import DTO.MusicDTO;
import com.pt.entity.Account;
import com.pt.entity.Category;
import com.pt.entity.Music;
import com.pt.repository.CategoryRepo;
import com.pt.repository.MusicRepo;
import com.pt.req.CreateMusicReq;
import com.pt.req.UpdateMusicReq;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/music")
@CrossOrigin(value = "*")
public class MusicApiController {
    private final MusicRepo musicRepo;

    private final ModelMapper modelMapper;

    private final CategoryRepo categoryRepo;


    public MusicApiController(MusicRepo musicRepo, ModelMapper modelMapper, CategoryRepo categoryRepo) {
        this.musicRepo = musicRepo;
        this.modelMapper = modelMapper;
        this.categoryRepo = categoryRepo;
    }

    @GetMapping("/show")
    public ResponseEntity<?> getAll(){
        List<Music> musicList= musicRepo.findAll();
        List<MusicDTO> musicDTOS=modelMapper.map(musicList,new TypeToken<List<MusicDTO>>(){}.getType());
        return new ResponseEntity<>(musicDTOS, HttpStatus.OK);
    }

    @GetMapping("/show/{id}")
    public ResponseEntity<?> getMusicById(@PathVariable Long id) {
        Optional<Music> musicOptional = musicRepo.findById(id);
        if (musicOptional.isPresent()) {
            Music music = musicOptional.get();
            MusicDTO musicDTO = modelMapper.map(music, MusicDTO.class);
            return ResponseEntity.ok(musicDTO);
        }
        return ResponseEntity.badRequest().body("Not found Song by id: " + id);
    }

    @GetMapping("/album/{categoryId}")
    public ResponseEntity<?> getMusicByCategoryId(@PathVariable Long categoryId) {
        List<Music> musicList = musicRepo.findByCategoryId(categoryId);

        if (!musicList.isEmpty()) {
            List<MusicDTO> musicDTOList = musicList.stream()
                    .map(music -> modelMapper.map(music, MusicDTO.class))
                    .collect(Collectors.toList());
            return ResponseEntity.ok(musicDTOList);
        }

        return ResponseEntity.notFound().build();
    }


    @PostMapping()
    public ResponseEntity<?> add(@RequestBody CreateMusicReq createMusicReq) {

        Optional<Music> opMusic = musicRepo.findByMusicName(createMusicReq.getMusicName());

        if (opMusic.isPresent()) {

            return new ResponseEntity<>("Song already exists", HttpStatus.BAD_REQUEST);
        }
        Optional<Category> opCategory = categoryRepo.findById(createMusicReq.getCategoryId());
        if (opCategory.isEmpty()) {
            return new ResponseEntity<>("Category does not exist", HttpStatus.BAD_REQUEST);
        }

        Music music = modelMapper.map(createMusicReq, Music.class);

        musicRepo.save(music);
        return new ResponseEntity<>("Add Song successfully: " + music.getMusicName(), HttpStatus.CREATED);
    }


    @PutMapping()
    public ResponseEntity<?> update( @RequestBody UpdateMusicReq updateMusicReq){
        Optional<Music> opMusic=musicRepo.findById(updateMusicReq.getId());
        if (opMusic.isPresent()){
            if (updateMusicReq.getAudioFilePath()==null||updateMusicReq.getAudioFilePath().equals("") || updateMusicReq.getMusicName()==null || updateMusicReq.getMusicName().equals("") ){
                return new ResponseEntity<>("Please enter enough information !!! not updated: "+ updateMusicReq.getId(),HttpStatus.BAD_REQUEST);
            }
            Music music=modelMapper.map(updateMusicReq,Music.class);


            music.setId(updateMusicReq.getId());
            musicRepo.save(music);
            return new ResponseEntity<>("Update successfully id: "+ updateMusicReq.getId(),HttpStatus.OK);

        }

        return new ResponseEntity<>("Song does not exist!!! not updated: "+ updateMusicReq.getId(),HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete( @PathVariable Long id){
        Optional<Music> opMusic=musicRepo.findById(id);
        if (opMusic.isPresent()){

            musicRepo.deleteById(id);
            return new ResponseEntity<>("delete successfully " + id, HttpStatus.OK);

        }

        return new ResponseEntity<>("Song does not exist!!! not delete",HttpStatus.BAD_REQUEST);
    }


@GetMapping("/search")
public ResponseEntity<?> searchMusicByName(@RequestParam String name) {
    Optional<Music> searchResults = musicRepo.findByMusicName(name);
    if (searchResults.isEmpty()) {
        return new ResponseEntity<>("Song does not exist", HttpStatus.BAD_REQUEST);
    }
    Music music = searchResults.get();
    MusicDTO musicDTO = modelMapper.map(music, MusicDTO.class);
    return ResponseEntity.ok(musicDTO);
}

}

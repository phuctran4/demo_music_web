package com.pt.controller.api;

import DTO.CategoryDTO;
import DTO.MusicDTO;
import com.pt.entity.Category;
import com.pt.entity.Music;
import com.pt.repository.CategoryRepo;
import com.pt.repository.MusicRepo;
import com.pt.req.CreateCategoryReq;
import com.pt.req.UpdateCategoryReq;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/category")
public class CategoryApiController {

    private final CategoryRepo categoryRepo;

    private ModelMapper modelMapper;

    private final MusicRepo musicRepo;


    public CategoryApiController(CategoryRepo categoryRepo, ModelMapper modelMapper,MusicRepo musicRepo ) {
        this.categoryRepo = categoryRepo;
        this.modelMapper = modelMapper;
        this.musicRepo = musicRepo;

    }

    @GetMapping("/show")
    public ResponseEntity<?> getAll(){
        List<Category> categories=categoryRepo.findAll();
        List<CategoryDTO> categoryDTOS=modelMapper.map(categories,new TypeToken<List<CategoryDTO>>(){}.getType());

        return ResponseEntity.ok(categoryDTOS);

    }
    @GetMapping("/show/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id){
        Optional<Category> categoryOptional=categoryRepo.findById(id);
        if (categoryOptional.isPresent()) {
            Category category = categoryOptional.get();
            CategoryDTO categoryDTO = modelMapper.map(category, CategoryDTO.class);
            return ResponseEntity.ok(categoryDTO);
        }
        return ResponseEntity.badRequest().body("Not found Category by id: " + id);

    }

    @PostMapping("/create")
    public ResponseEntity<?> add(@RequestBody CreateCategoryReq categoryReq ) {

        Optional<Category> optionalCategory = categoryRepo.findByCategoryName(categoryReq.getCategoryName());

        if (optionalCategory.isPresent()) {
            return new ResponseEntity<>("Category already exists", HttpStatus.BAD_REQUEST);
        }

        Category category=modelMapper.map(categoryReq,Category.class);
        categoryRepo.save(category);
        return new ResponseEntity<>("add new successfully", HttpStatus.OK);
    }
    @PutMapping("update/{id}")
    public ResponseEntity<?> update(@PathVariable Long id,@RequestBody UpdateCategoryReq updateCategoryReq){

        Optional<Category> optionalCategory=categoryRepo.findById(id);
        if (optionalCategory.isPresent()) {
            Category category=modelMapper.map(updateCategoryReq,Category.class);

            Optional<Category> opCategory1=categoryRepo.findByCategoryName(updateCategoryReq.getCategoryName());
            if (opCategory1.isPresent()){
                return new ResponseEntity<>("category name does exist!!! not updated "+ updateCategoryReq.getCategoryName(), HttpStatus.BAD_REQUEST);
            }
            category.setId(id);
            categoryRepo.save(category);
            return new ResponseEntity<>("Update successfully id: "+id, HttpStatus.OK);
        }

        return new ResponseEntity<>("category does not exist!!! not updated", HttpStatus.BAD_REQUEST);
    }
    @DeleteMapping("delete/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id){
        Optional<Category> optionalCategory=categoryRepo.findById(id);
        if (optionalCategory.isPresent()){
            categoryRepo.deleteById(id);
            return new ResponseEntity<>("delete successfully : "+ id,HttpStatus.OK);
        }
        return new ResponseEntity<>("id not found in table",HttpStatus.BAD_REQUEST);
    }
}

package com.pt.controller.page;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MusicController {
    @GetMapping("/music")
    public ModelAndView show(){
        ModelAndView modelAndView=new ModelAndView("music/music-index");
        return modelAndView;
    }
    @GetMapping("/edit-music")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ModelAndView edit(){
        ModelAndView modelAndView=new ModelAndView("music/edit-music");
        return modelAndView;
    }
    @GetMapping("/album")
    public ModelAndView album(){
        ModelAndView modelAndView=new ModelAndView("music/album");
        return modelAndView;
    }

}

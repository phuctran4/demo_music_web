package com.pt.controller.page;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {

    @GetMapping(value = {"", "/index", "/login"})
    public ModelAndView show(){
        ModelAndView modelAndView=new ModelAndView("login");
        return modelAndView;
    }
}

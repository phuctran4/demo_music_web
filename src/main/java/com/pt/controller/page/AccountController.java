package com.pt.controller.page;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AccountController {
    @GetMapping("listAccount")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ModelAndView show(){
        ModelAndView modelAndView=new ModelAndView("fe_account/account_index");
        return modelAndView;
    }
}

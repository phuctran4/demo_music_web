package com.pt.repository;

import com.pt.entity.Music;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MusicRepo extends JpaRepository<Music,Long> {
    Optional<Music> findByMusicName(String musicName);

    List<Music> findByCategoryId(Long categoryId);
}

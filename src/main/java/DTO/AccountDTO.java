package DTO;

import com.pt.entity.Role;
import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;


@Data
@NoArgsConstructor
public class AccountDTO {
    private Long id;

    @NotBlank
    private String username;

    private String email;

    private String fullName;

    private Role role;
}

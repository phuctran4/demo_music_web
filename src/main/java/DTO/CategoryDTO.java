package DTO;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class CategoryDTO {
    private Long id;

    @NotNull
    private String categoryName;
}

package DTO;

import com.pt.entity.Category;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;


@Data
@NoArgsConstructor
public class MusicDTO {


    private Long id;

    @NotNull
    private String musicName;

    private String categoryName;

    private Long categoryId;

    private String artist;

    @NotNull
    private String audioFilePath;

    private String imgFilePath;



}
